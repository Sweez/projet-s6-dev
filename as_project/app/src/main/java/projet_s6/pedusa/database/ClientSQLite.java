package projet_s6.pedusa.database;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import projet_s6.pedusa.R;
import projet_s6.pedusa.structure.Jeu;


// Client permettant de realiser des requetes sur la DB SQLite
public class ClientSQLite extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME = "pedusa.db";

    // Creation des tables
    private final String SQL_CREATE_JEU = "CREATE TABLE Jeu (nom TEXT, vecteur INTEGER, nbUtilisation INTEGER, PRIMARY KEY(nom));";


    // Suppression des tables
    private final String SQL_REMOVE_JEU = "DROP TABLE IF EXISTS Jeu;";


    public ClientSQLite(Context context) {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
        getReadableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_JEU);
    }

    public void creerDB(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(SQL_CREATE_JEU);
        db.close();
    }

    public boolean dataBaseCreer(){
        SQLiteDatabase checkDB = null;
        try{
            checkDB = SQLiteDatabase.openDatabase("//data/data/projet_s6.pedusa/databases/"+DATABASE_NAME,null,SQLiteDatabase.OPEN_READONLY);
            checkDB.close();
            System.out.println("DATABASE déjà présente");
            return true;
        }catch (SQLiteException e){
            System.out.println("DATABASE non présente");
            return false;
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_REMOVE_JEU);
        this.onCreate(db);
    }

    public void insertionJeux(){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<String> nomJeu = new ArrayList<>();
        Cursor request = db.rawQuery("SELECT nom FROM Jeu",null);
        if (request.moveToFirst()){
            do {
                nomJeu.add(request.getString(0));
            }while(request.moveToNext());
        }
        request.close();
        db.close();

        db = this.getWritableDatabase();
        if (!nomJeu.contains("Snake")) {
            db.execSQL("INSERT INTO Jeu (nom,vecteur,nbUtilisation) VALUES ('Snake'," + R.drawable.game_snake_vector + ",0);");
        }
    }

    public ArrayList<Jeu> getAllJeux(){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor request = db.rawQuery("SELECT nom,vecteur FROM Jeu",null);
        ArrayList<Jeu> jeux = new ArrayList<>();
        if (request.moveToFirst()){
            do {
                System.out.println(request.getString(1));
                jeux.add(new Jeu(request.getString(0),request.getInt(1)));
            }while(request.moveToNext());
        }
        request.close();
        db.close();
        return jeux;
    }

    public ArrayList<Jeu> getTopPlayedJeux(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor request = db.rawQuery("SELECT nom,vecteur,nbUtilisation FROM Jeu WHERE nbUtilisation > 0 ORDER BY nbUtilisation desc",null);
        ArrayList<Jeu> jeux = new ArrayList<>();
        if (request.moveToFirst()){
            do {
                jeux.add(new Jeu(request.getString(0),request.getInt(1)));
            }while(request.moveToNext());
        }
        request.close();
        db.close();
        return jeux;
    }

    public ArrayList<Jeu> getNewJeux(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor request = db.rawQuery("SELECT nom,vecteur FROM Jeu WHERE nbUtilisation = 0",null);
        ArrayList<Jeu> jeux = new ArrayList<>();
        if (request.moveToFirst()){
            do {
                jeux.add(new Jeu(request.getString(0),request.getInt(1)));
            }while(request.moveToNext());
        }
        request.close();
        db.close();
        return jeux;
    }

    public void incrementUtilisation(String nomJeu){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE Jeu SET nbUtilisation = nbUtilisation + 1 WHERE nom = '"+nomJeu+"'");
        db.close();
    }

}
