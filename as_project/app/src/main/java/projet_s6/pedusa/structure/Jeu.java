package projet_s6.pedusa.structure;

public class Jeu {

    private String nom;
    private int vector;

    public Jeu (String nom, int vector){
        this.nom = nom;
        this.vector = vector;
    }

    public String getNom(){
        return this.nom;
    }

    public int getVector(){
        return this.vector;
    }

}
