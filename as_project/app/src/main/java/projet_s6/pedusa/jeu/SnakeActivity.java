package projet_s6.pedusa.jeu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Timer;

import projet_s6.pedusa.MainActivity;
import projet_s6.pedusa.R;

import projet_s6.pedusa.snake.*;

public class SnakeActivity extends AppCompatActivity{

    private SnakeCanvas canvas;
    private SnakeController controller;
    private Timer timer;

    private int maxScore;

    private final SnakeActivity ACTIVITY = this;
    private final int FLAGS_HIDE_NAVBAR = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
            | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LOW_PROFILE;

    private float prevX,prevY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snake);
        this.maxScore = 0;
        // On rend invisible la barre de naviation

        getWindow().getDecorView().setSystemUiVisibility(FLAGS_HIDE_NAVBAR);

        // Code below is to handle presses of Volume up or Volume down.
        // Without this, after pressing volume buttons, the navigation bar will
        // show up and won't hide
        final View decorView = getWindow().getDecorView();
        decorView
                .setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
                {

                    @Override
                    public void onSystemUiVisibilityChange(int visibility)
                    {
                        decorView.setSystemUiVisibility(FLAGS_HIDE_NAVBAR);
                    }
                });


        this.canvas = new SnakeCanvas(this,this);
        setContentView(this.canvas);

        this.controller = new SnakeController(this,this.canvas);
        this.timer = new Timer(true);
        this.timer.scheduleAtFixedRate(this.controller,0,300);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // On rend invisible la bare de naviation
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(FLAGS_HIDE_NAVBAR);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(FLAGS_HIDE_NAVBAR);
    }

    public void displayChangements(SnakeCanvas canvas){
        setContentView(canvas);
    }

    public void defaite(int score){
        this.timer.cancel();

        if(score>maxScore){
            this.maxScore = score;
        }

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View popUpView = inflater.inflate(R.layout.pop_up_defaite_snake,null);

        int width = RelativeLayout.LayoutParams.WRAP_CONTENT;
        int height = RelativeLayout.LayoutParams.WRAP_CONTENT;
        final PopupWindow popupWindow = new PopupWindow(popUpView,width,height,false);
        popupWindow.showAtLocation(popUpView, Gravity.CENTER, 0, 0);


        TextView actualScore = (TextView) popUpView.findViewById(R.id.text_score);
        actualScore.setText("Votre score : "+score); //// SCORE

        TextView bestSCore = (TextView) popUpView.findViewById(R.id.text_best_score);
        bestSCore.setText("Meilleur score : "+maxScore); //// BEST SCORE

        Button boutonReplay = (Button) popUpView.findViewById(R.id.boutton_replay);
        boutonReplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                canvas = new SnakeCanvas(v.getContext(),ACTIVITY);
                setContentView(canvas);

                controller = new SnakeController(ACTIVITY,canvas);
                timer = new Timer(true);
                timer.scheduleAtFixedRate(controller,0,300);
                popupWindow.dismiss();
            }
        });

        Button boutonExit = (Button) popUpView.findViewById(R.id.button_exit);
        boutonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(),MainActivity.class);
                startActivity(intent);
            }
        });
    }

    public void changementDirection(Direction direction){
        System.out.println("(Activity) - changement de direction");
        this.controller.changementDirection(direction);
    }

    @Override
    public void onBackPressed() {
        this.timer.cancel();
        super.onBackPressed();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN){
            System.out.println("ACTION DOWN");
            prevX = ev.getX();
            prevY = ev.getY();
            return true;
        }else if(ev.getAction() == MotionEvent.ACTION_UP){
            System.out.println("SWIPE ENDED DETECTED");
            float newX = ev.getX();
            float newY = ev.getY();
            System.out.println("SWIPE coordX = "+Float.toString(newX)+" - coordY = "+Float.toString(newY));
            //Calculates where we swiped
            if (Math.abs(newX - prevX) > Math.abs(newY - prevY)) {
                //LEFT - RiGHT Direction
                if( newX > prevX) {
                    //RIGHT
                    System.out.println("SWIPE RIGHT DETECTED");
                    this.changementDirection(Direction.RIGHT);
                } else {
                    //LEFT
                    System.out.println("SWIPE LEFT DETECTED");
                    this.changementDirection(Direction.LEFT);
                }
            } else {
                // UP-DOWN Direction
                if (newY > prevY) {
                    //DOWN
                    System.out.println("SWIPE DOWN DETECTED");
                    this.changementDirection(Direction.DOWN);
                } else {
                    //UP
                    System.out.println("SWIPE UP DETECTED");
                    this.changementDirection(Direction.UP);
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }
}
