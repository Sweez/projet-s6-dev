package projet_s6.pedusa.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import projet_s6.pedusa.R;
import projet_s6.pedusa.structure.Jeu;

public class JeuAdapter extends ArrayAdapter<Jeu> {

    private static List<Jeu> originalList;

    public JeuAdapter(Context context, List<Jeu> jeuxList) {
        super(context, 0, jeuxList);
        this.originalList = jeuxList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @Nullable ViewGroup parent){
        return initView(position,convertView,parent);
    }

    private View initView(int position, View convertView, ViewGroup parent){

        if (convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.jeu_view,parent,false);
        }

        ImageView jeuVector = convertView.findViewById(R.id.jeuImage);
        jeuVector.setImageResource(R.drawable.game_snake_vector);

        Jeu jeuItem = getItem(position);


        if (jeuItem!=null){
            System.out.println("SET IMAGE : "+Integer.toString(jeuItem.getVector()));
            jeuVector.setImageResource(jeuItem.getVector());
        }

        return jeuVector;
    }

    public ArrayList<Jeu> filter(String filtre){
        ArrayList<Jeu> jeux = new ArrayList<>();
        for (Jeu item : originalList) {
            if (item.getNom().toLowerCase().contains(filtre)){
                jeux.add(item);
            }
        }
        return jeux;
    }

}
