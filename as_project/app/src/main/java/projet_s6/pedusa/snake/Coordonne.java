package projet_s6.pedusa.snake;

public class Coordonne {

    private int x;
    private int y;

    public Coordonne(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX(){
        return this.x;
    }

    public int getY(){
        return this.y;
    }
}
