package projet_s6.pedusa.snake;

public class SnakeModel {

    private ArrayQueue<Coordonne> snake;
    private Coordonne fruit;
    private int[][] plateau;
    private Direction snakeDirection;

    private boolean directionBloque;

    public SnakeModel(){
        this.snake = new ArrayQueue<>();
        this.fruit = new Coordonne(0,0);
        this.plateau = new int[16][30];
        this.snakeDirection = Direction.LEFT;

        this.directionBloque = false;

        for (int i = 0; i < this.plateau.length; i++){
            for (int y = 0; y < this.plateau[0].length; y++){
                if(i==0 || i==this.plateau.length-1 || y == 0 || y == this.plateau[0].length-1){
                    this.plateau[i][y] = 1;
                }else {
                    this.plateau[i][y] = 0;
                }
            }
        }

        for (int i = 0; i <= 3; i++){
            this.snake.add(new Coordonne(this.plateau.length/2,(this.plateau[0].length/2)+i));
            this.plateau[this.plateau.length/2][(this.plateau[0].length/2)+i] = 1;
        }

        this.fruit = apparitionFruit();
    }

    public int deplacementValide(Coordonne cible){
        if (this.plateau[cible.getX()][cible.getY()] == 1){
            if (this.snake.contains(cible) && this.snake.getIndex(cible) != this.snake.length()-2){
                return -1;
            }else {
                if (!this.snake.contains(cible)){
                    return -1;
                }else{
                    return 0;
                }
            }
        }else {
            return 1;
        }
    }

    public String deplacerSnake(){
        // print snake
        String chaine = "";
        for(int i = 0; i < this.snake.length(); i++){
            chaine += "("+this.snake.elementAt(i).getX()+","+this.snake.elementAt(i).getY()+") ";
        }
        System.out.println(chaine);


        Coordonne tmp = this.snake.element();
        switch (this.snakeDirection) {
            case RIGHT:
                tmp = new Coordonne(tmp.getX(), tmp.getY() + 1);
                break;
            case LEFT:
                tmp = new Coordonne(tmp.getX(), tmp.getY() - 1);
                break;
            case UP:
                tmp = new Coordonne(tmp.getX() - 1, tmp.getY());
                break;
            case DOWN:
                tmp = new Coordonne(tmp.getX() + 1, tmp.getY());
                break;
            default:
                break;
        }
        this.directionBloque = false;
        if(this.deplacementValide(tmp) == 1){
            this.checkMangeFruit(tmp);
            Coordonne celluleDefiler = this.snake.remove();
            this.snake.insert(tmp);
            if(celluleDefiler.getX()!=-1 && celluleDefiler.getY()!=-1){
                this.plateau[celluleDefiler.getX()][celluleDefiler.getY()] = 0;
            }
            this.plateau[tmp.getX()][tmp.getY()] = 1;
            return "OK";
        }else if (this.deplacementValide(tmp) == -1){
            return "DEFAITE";
        }else {
            return "RIEN";
        }
    }

    private void checkMangeFruit(Coordonne cible) {
        if(this.plateau[cible.getX()][cible.getY()] == 2){
            this.plateau[cible.getX()][cible.getY()] = 0;
            this.extensionSnake();
            this.fruit = this.apparitionFruit();
        }
    }

    public void extensionSnake() {
        this.snake.add(new Coordonne(-1,-1));
    }

    public Coordonne apparitionFruit(){
        int x = 0;
        int y = 0;
        while (this.plateau[x][y] == 1) {
            x = (int) (Math.random() * ((this.plateau.length - 1))) + 1;
            y = (int) (Math.random() * ((this.plateau[0].length - 1))) + 1;
        }
        this.plateau[x][y] = 2;
        return new Coordonne(x,y);
    }

    public void changementDirection(Direction direction){
        if (!this.directionBloque){
            switch (direction){
                case RIGHT:
                    if (this.snakeDirection != Direction.LEFT){
                        System.out.println("(Model) - changement de direction : RIGHT");
                        this.snakeDirection = Direction.RIGHT;
                        this.directionBloque = true;
                    }
                    break;
                case LEFT:
                    if (this.snakeDirection != Direction.RIGHT){
                        System.out.println("(Model) - changement de direction : LEFT");
                        this.snakeDirection = Direction.LEFT;
                        this.directionBloque = true;
                    }
                    break;
                case UP:
                    if (this.snakeDirection != Direction.DOWN){
                        System.out.println("(Model) - changement de direction : UP");
                        this.snakeDirection = Direction.UP;
                        this.directionBloque = true;
                    }
                    break;
                case DOWN:
                    if (this.snakeDirection != Direction.UP){
                        System.out.println("(Model) - changement de direction : DOWN");
                        this.snakeDirection = Direction.DOWN;
                        this.directionBloque = true;
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public Coordonne getFruit(){
        return this.fruit;
    }

    public ArrayQueue<Coordonne> getSnake(){
        return this.snake;
    }
}
