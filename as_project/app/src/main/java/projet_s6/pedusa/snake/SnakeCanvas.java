package projet_s6.pedusa.snake;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

import projet_s6.pedusa.R;
import projet_s6.pedusa.jeu.SnakeActivity;


public class SnakeCanvas extends View {

    private Paint grille;
    private Paint mur;
    private Paint cadrillage;
    private Paint fruit;
    private Paint serpent;
    private Paint teteSerpent;
    private Paint cellule;
    private SnakeActivity activity;
    private float prevX,prevY;

    private final int TAILLE_RECTANGLE = 60;

    private Coordonne coordFruit;
    private ArrayQueue<Coordonne> snake;

    public SnakeCanvas(Context context, SnakeActivity activity) {
        super(context);
        this.grille = new Paint();
        this.grille.setColor(getResources().getColor(R.color.bordure_snake));
        this.grille.setStrokeWidth(3);

        this.mur = new Paint();
        this.mur.setColor(getResources().getColor(R.color.color_wall));
        this.mur.setStrokeWidth(2);

        this.cadrillage = new Paint();
        this.cadrillage.setStyle(Paint.Style.STROKE);
        this.cadrillage.setColor(Color.WHITE);
        this.cadrillage.setStrokeWidth(2);

        this.fruit = new Paint();
        this.fruit.setColor(Color.WHITE);
        this.fruit.setStrokeWidth(2);

        this.serpent = new Paint();
        this.serpent.setColor(getResources().getColor(R.color.snake_body));
        this.serpent.setStrokeWidth(2);

        this.teteSerpent = new Paint();
        this.teteSerpent.setColor(getResources().getColor(R.color.color_snake_head));
        this.teteSerpent.setStrokeWidth(2);

        this.cellule = new Paint();
        this.cellule.setColor(getResources().getColor(R.color.plateau_snake));
        this.cellule.setStrokeWidth(2);

        this.coordFruit = new Coordonne(-1,-1);
        this.snake = new ArrayQueue<>();

        this.activity = activity;
    }

    @Override
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        canvas.drawRect(0,0,canvas.getWidth(),canvas.getHeight(),this.grille);

        initialiserGrille(canvas);
        // draw le fruit
        if (this.coordFruit.getX() != -1){
            int x = this.coordFruit.getX()*TAILLE_RECTANGLE + TAILLE_RECTANGLE;
            int y = this.coordFruit.getY()*TAILLE_RECTANGLE + TAILLE_RECTANGLE;
            canvas.drawRect(y, x, y + TAILLE_RECTANGLE,x + TAILLE_RECTANGLE
                            ,this.fruit);
        }
        // draw le snake
        if (this.snake.length()>0){
            Coordonne tmp = this.snake.elementAt(0);
            int x = tmp.getX()*TAILLE_RECTANGLE + TAILLE_RECTANGLE;
            int y = tmp.getY()*TAILLE_RECTANGLE + TAILLE_RECTANGLE;
            canvas.drawRect(y, x, y + TAILLE_RECTANGLE,
                    x + TAILLE_RECTANGLE, this.teteSerpent);

            for (int i = 1; i<this.snake.length(); i++){
                tmp = this.snake.elementAt(i);
                x = tmp.getX()*TAILLE_RECTANGLE + TAILLE_RECTANGLE;
                y = tmp.getY()*TAILLE_RECTANGLE + TAILLE_RECTANGLE;
                canvas.drawRect(y, x, y + TAILLE_RECTANGLE,
                        x + TAILLE_RECTANGLE, this.serpent);
            }
        }

        for (int x = TAILLE_RECTANGLE; x < (1920-TAILLE_RECTANGLE); x+=TAILLE_RECTANGLE) {
            for(int y = TAILLE_RECTANGLE; y < (1080 - TAILLE_RECTANGLE); y+=TAILLE_RECTANGLE){
                if (y==TAILLE_RECTANGLE || y == (1080 - 2*TAILLE_RECTANGLE) || x == TAILLE_RECTANGLE
                        || x == (1920 - 2*TAILLE_RECTANGLE)) {
                    canvas.drawRect(x, y, x + TAILLE_RECTANGLE, y + TAILLE_RECTANGLE, this.cadrillage);
                }
            }
        }
    }

    private void initialiserGrille(Canvas canvas){
        for (int x = TAILLE_RECTANGLE; x < (1920-TAILLE_RECTANGLE); x+=TAILLE_RECTANGLE) {
            for(int y = TAILLE_RECTANGLE; y < (1080 - TAILLE_RECTANGLE); y+=TAILLE_RECTANGLE){
                if (y==TAILLE_RECTANGLE || y == (1080 - 2*TAILLE_RECTANGLE) || x == TAILLE_RECTANGLE
                || x == (1920 - 2*TAILLE_RECTANGLE)){
                    canvas.drawRect(x, y, x + TAILLE_RECTANGLE, y + TAILLE_RECTANGLE, this.mur);
                }else{
                    canvas.drawRect(x, y, x + TAILLE_RECTANGLE, y + TAILLE_RECTANGLE, this.cellule);
                }
            }
        }
    }

    public void setSnake(ArrayQueue<Coordonne> snake){
        this.snake = snake;
    }

    public void setFruit(Coordonne coordFruit){
        this.coordFruit = coordFruit;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN){
            System.out.println("ACTION DOWN");
            prevX = event.getX();
            prevY = event.getY();
            return true;
        }else if(event.getAction() == MotionEvent.ACTION_UP
                || event.getAction() == MotionEvent.ACTION_CANCEL){
            System.out.println("SWIPE ENDED DETECTED");
            float newX = event.getX();
            float newY = event.getY();
            System.out.println("SWIPE coordX = "+Float.toString(newX)+" - coordY = "+Float.toString(newY));
            //Calculates where we swiped
            if (Math.abs(newX - prevX) > Math.abs(newY - prevY)) {
                //LEFT - RiGHT Direction
                if( newX > prevX) {
                    //RIGHT
                    System.out.println("SWIPE RIGHT DETECTED");
                    this.activity.changementDirection(Direction.RIGHT);
                } else {
                    //LEFT
                    System.out.println("SWIPE LEFT DETECTED");
                    this.activity.changementDirection(Direction.LEFT);
                }
            } else {
                // UP-DOWN Direction
                if (newY > prevY) {
                    //DOWN
                    System.out.println("SWIPE DOWN DETECTED");
                    this.activity.changementDirection(Direction.DOWN);
                } else {
                    //UP
                    System.out.println("SWIPE UP DETECTED");
                    this.activity.changementDirection(Direction.UP);
                }
            }
        }
        return false;
    }

}
