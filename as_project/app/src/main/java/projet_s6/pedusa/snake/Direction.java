package projet_s6.pedusa.snake;

public enum Direction {
    LEFT,
    RIGHT,
    UP,
    DOWN
}
