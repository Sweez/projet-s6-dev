package projet_s6.pedusa.snake;

import java.util.TimerTask;

import projet_s6.pedusa.jeu.SnakeActivity;

public class SnakeController extends TimerTask {

    private SnakeModel model;
    private SnakeActivity activity;
    private SnakeCanvas canvas;

    public SnakeController(SnakeActivity activity, SnakeCanvas canvas){
        this.model = new SnakeModel();
        this.activity = activity;
        this.canvas = canvas;

        this.canvas.setFruit(this.model.getFruit());
        this.canvas.setSnake(this.model.getSnake());
        this.activity.setContentView(this.canvas);
    }

    @Override
    public void run() {
        String etat = this.model.deplacerSnake();
        if (etat=="OK"){
            // MAJ de la vue
            this.canvas.setFruit(this.model.getFruit());
            this.canvas.setSnake(this.model.getSnake());
            this.activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    activity.displayChangements(canvas);
                }
            });

        }else if (etat == "DEFAITE"){
            this.activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    activity.defaite(model.getSnake().length());
                }
            });
        }
    }

    public void changementDirection(Direction direction){
        System.out.println("(Controller) - changement de direction");
        this.model.changementDirection(direction);
    }

}
