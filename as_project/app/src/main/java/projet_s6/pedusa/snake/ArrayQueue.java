package projet_s6.pedusa.snake;

import java.util.ArrayList;

public class ArrayQueue<Object> {

    private ArrayList<Object> queue;

    public ArrayQueue(){
        this.queue = new ArrayList<>();
    }

    public void add(Object o){
        this.queue.add(o);
    }

    public void insert(Object o){
        this.queue.add(0,o);
    }

    public Object remove(){
        Object o = this.queue.get(this.queue.size()-1);
        this.queue.remove(this.queue.size()-1);
        return o;
    }

    public boolean contains(Object o){
        return this.queue.contains(o);
    }

    public int getIndex(Object o){
        return this.queue.indexOf(o);
    }

    public int length(){
        return this.queue.size();
    }

    public Object element(){
        return this.queue.get(0);
    }

    public Object elementAt(int index){
        return this.queue.get(index);
    }
}
