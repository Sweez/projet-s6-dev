package projet_s6.pedusa;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import projet_s6.pedusa.adapter.JeuAdapter;
import projet_s6.pedusa.database.ClientSQLite;
import projet_s6.pedusa.structure.Jeu;

public class MainActivity extends AppCompatActivity {

    private JeuAdapter jeuAdapter;
    private ArrayList<Jeu> jeuxAfficher;

    private static ClientSQLite bdd;
    private ListView listJeux;

    private Button boutonAllGame;
    private Button boutonTopGame;
    private Button boutonNewGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.creerDatabase();

        // On recupere les jeux dispo
        this.jeuxAfficher = bdd.getAllJeux();
        // On recupere la view pour les jeux
        this.listJeux = findViewById(R.id.affichageRecherche);
        this.jeuAdapter = new JeuAdapter(this,this.jeuxAfficher);
        this.displayJeux(this.jeuxAfficher);

        // Initialisation des boutons de filtrage des jeux
        this.boutonAllGame = findViewById(R.id.boutonAllGame);
        this.boutonAllGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // On recupere les jeux dispo
                jeuxAfficher = bdd.getAllJeux();
                jeuAdapter = new JeuAdapter(v.getContext(),jeuxAfficher);
                // On recupere la view pour les jeux
                displayJeux(jeuxAfficher);
            }
        });

        this.boutonTopGame = findViewById(R.id.boutonTopGame);
        this.boutonTopGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // On recupere les jeux dispo
                jeuxAfficher = bdd.getTopPlayedJeux();
                jeuAdapter = new JeuAdapter(v.getContext(),jeuxAfficher);
                // On recupere la view pour les jeux
                displayJeux(jeuxAfficher);
            }
        });

        this.boutonNewGame = findViewById(R.id.boutonNewGame);
        this.boutonNewGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // On recupere les jeux dispo
                jeuxAfficher = bdd.getNewJeux();
                jeuAdapter = new JeuAdapter(v.getContext(),jeuxAfficher);
                // On recupere la view pour les jeux
                displayJeux(jeuxAfficher);
            }
        });

        this.listJeux = findViewById(R.id.affichageRecherche);
        this.listJeux.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Jeu selectedGame = (Jeu) listJeux.getItemAtPosition(position);
                bdd.incrementUtilisation(selectedGame.getNom());
                System.out.println("Incrementation du nombre d'utilisation du jeu : "+selectedGame.getNom());
                try {
                    Class<?> c = Class.forName("projet_s6.pedusa.jeu."+selectedGame.getNom()+"Activity");
                    Intent intent = new Intent(view.getContext(), c);
                    startActivity(intent);
                } catch (ClassNotFoundException ignored) {
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        // Recuperation du menu
        getMenuInflater().inflate(R.menu.menu_recherche, menu);
        MenuItem menuItem = menu.findItem(R.id.search_icon);

        // On ajoute la searchView a l'icone
        SearchView searchView = (SearchView) menuItem.getActionView();

        SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchAutoComplete.setHintTextColor(getResources().getColor(android.R.color.darker_gray));

        searchView.setQueryHint(getResources().getString(R.string.recherche_int));
        // Modification du rendu du bouton de suppression de texte
        ImageView boutonSuppr = searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
        boutonSuppr.setImageResource(R.drawable.ic_close_black_24dp);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                this.onQueryTextChange(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText.isEmpty()){
                    displayJeux(jeuxAfficher);
                }else {
                    ArrayList<Jeu> tmp = jeuAdapter.filter((newText));
                    displayJeux(tmp);
                }
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    private void creerDatabase(){
        //Creation de la DB
        bdd = new ClientSQLite(this);
        bdd.insertionJeux();
        if (!bdd.dataBaseCreer()) {
            System.out.println("DATABASE creation en cours");
            bdd.creerDB();
            bdd.insertionJeux();
        }else {
            System.out.println("DATABASE deja presente et data presente");
            SQLiteDatabase db = bdd.getReadableDatabase();
            Cursor request = db.rawQuery("SELECT nom FROM Jeu",null);
            if (request.moveToFirst()){
                do {
                    System.out.println("DATABE - JEU : "+request.getString(0));
                }while(request.moveToNext());
            }
            request.close();
            db.close();
        }
    }


    private void displayJeux(List<Jeu> jeuxList){
        this.listJeux.setAdapter(new JeuAdapter(this,jeuxList));
    }
}
